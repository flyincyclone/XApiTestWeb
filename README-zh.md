# XApiTestWeb

> 这是一个从以vue admin template为模板,制作的XApiTest的前端XApiTestWeb，需要和XApiTest配合使用。
> 它只包含了 Element UI & axios & iconfont & permission control & lint，这些搭建后台必要的东西。


[线上地址-国内访问](https://gitee.com/flyincyclone/admin-httpapi)

目前版本为 `v4.0+` 基于 `vue-cli` 进行构建，若你想使用旧版本，可以切换分支到[tag/3.11.0](https://github.com/PanJiaChen/vue-admin-template/tree/tag/3.11.0)，它不依赖 `vue-cli`。

## Extra

如果你想要根据用户角色来动态生成侧边栏和 router，你可以使用该分支[permission-control](https://github.com/PanJiaChen/vue-admin-template/tree/permission-control)

## 相关项目

- gitte:[vue-element-admin](https://gitee.com/panjiachen/vue-admin-template)
- github:[vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)
- [httapi](https://gitee.com/flyincyclone/admin-httpapi)

写了一个系列的教程配套文章，如何从零构建后一个完整的后台项目:

## Build Setup

```bash
# 克隆项目
git clone https://gitee.com/flyincyclone/admin-httpapi.git

# 进入项目目录
cd admin-httpapi

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 其它

```bash
# 预览发布环境效果
npm run preview

# 预览发布环境效果 + 静态资源分析
npm run preview -- --report

# 代码格式检查
npm run lint

# 代码格式检查并自动修复
npm run lint -- --fix
```

更多信息请参考 [使用文档](https://panjiachen.github.io/vue-element-admin-site/zh/)

## Demo

![demo](https://github.com/PanJiaChen/PanJiaChen.github.io/blob/master/images/demo.gif)

## Browsers support

Modern browsers and Internet Explorer 10+.
