import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/Login.vue'),
    hidden: true
  },

  {
    path: '/register',
    component: () => import('@/views/register/Register.vue'),
    hidden: true
  },

  {
    path: '/changepassword',
    component: Layout,
    name: 'ChangePassword',
    children: [
      {
        path: '/changepassword',
        name: 'changepassword',
        component: () => import('@/views/register/ChangePassword.vue'),
        meta: { title: '修改密码', icon: 'form' },
        hidden: true
      }
    ]
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/run_debug/json_report',
    component: () => import('@/views/test_report/ViewJsonReport.vue'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: 'dashboard',
    name: 'Dashboard',
    children: [{
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/Home.vue'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },
  {
    path: '/project',
    component: Layout,
    redirect: 'project_list',
    name: 'Project',
    meta: { title: '项目管理', icon: 'el-icon-goods' },
    children: [
      {
        path: 'project_list',
        name: 'project_list',
        component: () => import('@/views/project/ProjectList.vue'),
        meta: { title: '项目列表', icon: 'table' }
      },
      {
        path: 'create_project',
        name: 'create_project',
        component: () => import('@/views/project/CreateProject.vue'),
        meta: { title: '新建项目', icon: 'form' }
      },
      {
        path: 'update_project',
        name: 'update_project',
        component: () => import('@/views/project/CreateProject.vue'),
        meta: { title: '编辑项目', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/module',
    component: Layout,
    redirect: 'module_list',
    name: 'Module',
    meta: { title: '模块管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'module_list',
        name: 'module_list',
        component: () => import('@/views/module/ModuleList.vue'),
        meta: { title: '模块列表', icon: 'table' }
      },
      {
        path: 'create_module',
        name: 'create_module',
        component: () => import('@/views/module/CreateModule.vue'),
        meta: { title: '新建模块', icon: 'form' }
      },
      {
        path: 'update_module',
        name: 'update_module',
        component: () => import('@/views/module/CreateModule.vue'),
        meta: { title: '编辑模块', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/config',
    component: Layout,
    redirect: 'config_list',
    name: 'Config',
    meta: { title: '配置管理', icon: 'el-icon-s-tools' },
    children: [
      {
        path: 'config_list',
        name: 'config_list',
        component: () => import('@/views/config/ConfigList.vue'),
        meta: { title: '配置列表', icon: 'table' }
      },
      {
        path: 'create_config',
        name: 'create_config',
        component: () => import('@/views/config/CreateConfig.vue'),
        meta: { title: '新建配置', icon: 'form' }
      },
      {
        path: 'import_config',
        name: 'import_config',
        component: () => import('@/views/config/ImportConfig.vue'),
        meta: { title: '导入配置', icon: 'form' },
        hidden: true
      },
      {
        path: 'update_config',
        name: 'update_config',
        component: () => import('@/views/config/CreateConfig.vue'),
        meta: { title: '编辑配置', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/test_auth',
    component: Layout,
    redirect: 'auth_list',
    name: 'TestAuth',
    meta: { title: '用例认证', icon: 'el-icon-user-solid' },
    children: [
      {
        path: 'auth_list',
        name: 'auth_list',
        component: () => import('@/views/test_auth/AuthList.vue'),
        meta: { title: '认证列表', icon: 'table' }
      },
      {
        path: 'create_auth',
        name: 'create_auth',
        component: () => import('@/views/test_auth/CreateAuth.vue'),
        meta: { title: '新建认证', icon: 'form' }
      },
      {
        path: 'update_auth',
        name: 'update_auth',
        component: () => import('@/views/test_auth/CreateAuth.vue'),
        meta: { title: '编辑认证', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/test_case',
    component: Layout,
    redirect: 'case_list',
    name: 'TestCase',
    meta: { title: '测试用例', icon: 'el-icon-document' },
    children: [
      {
        path: 'case_list',
        name: 'case_list',
        component: () => import('@/views/test_case/CaseList.vue'),
        meta: { title: '用例列表', icon: 'table' }
      },
      {
        path: 'create_case',
        name: 'create_case',
        component: () => import('@/views/test_case/CreateCase.vue'),
        meta: { title: '新建用例', icon: 'form' }
      },
      {
        path: 'update_case',
        name: 'update_case',
        component: () => import('@/views/test_case/CreateCase.vue'),
        meta: { title: '编辑用例', icon: 'form' },
        hidden: true
      }
    ]
  },
  {
    path: '/test_suite',
    component: Layout,
    redirect: 'suite_list',
    name: 'TestSuite',
    meta: { title: '测试套件', icon: 'el-icon-receiving' },
    children: [
      {
        path: 'suite_list',
        name: 'suite_list',
        component: () => import('@/views/test_suite/SuiteList.vue'),
        meta: { title: '套件列表', icon: 'table' }
      },
      {
        path: 'create_suite',
        name: 'create_suite',
        component: () => import('@/views/test_suite/CreateSuite.vue'),
        meta: { title: '新建套件', icon: 'form' }
      },
      {
        path: 'update_suite',
        name: 'update_suite',
        hidden: true,
        component: () => import('@/views/test_suite/CreateSuite.vue'),
        meta: { title: '编辑套件', icon: 'form' }
      }
    ]
  },
  {
    path: '/test_schedule',
    component: Layout,
    redirect: 'schedule_list',
    name: 'TestSchedule',
    meta: { title: '定时任务', icon: 'el-icon-receiving' },
    children: [
      {
        path: 'schedule_list',
        name: 'schedule_list',
        component: () => import('@/views/schedule/ScheduleList.vue'),
        meta: { title: '任务列表', icon: 'table' }
      },
      {
        path: 'create_schedule',
        name: 'create_schedule',
        component: () => import('@/views/schedule/CreateSchedule.vue'),
        meta: { title: '新建任务', icon: 'form' }
      },
      {
        path: 'update_schedule',
        name: 'update_schedule',
        hidden: true,
        component: () => import('@/views/schedule/CreateSchedule.vue'),
        meta: { title: '编辑任务', icon: 'form' }
      }
    ]
  },
  {
    path: '/file',
    component: Layout,
    redirect: 'file_list',
    name: 'File',
    meta: { title: '关联文件', icon: 'el-icon-folder' },
    children: [
      {
        path: 'file_list',
        name: 'file_list',
        component: () => import('@/views/file/FileList.vue'),
        meta: { title: '文件列表', icon: 'el-icon-files' }
      },
      {
        path: 'upload_file',
        name: 'upload_file',
        component: () => import('@/views/file/UploadFile.vue'),
        meta: { title: '上传文件', icon: 'el-icon-upload' }
      }
    ]
  },
  {
    path: '/report',
    component: Layout,
    redirect: 'report_list',
    name: 'Report',
    children: [
      {
        path: 'report_list',
        name: 'report_list',
        component: () => import('@/views/test_report/ReportList.vue'),
        meta: { title: '查看报告', icon: 'table' }
      }
    ]
  },
  {
    path: '/log',
    component: Layout,
    redirect: 'debug_log',
    name: 'Log',
    meta: { title: '日志管理', icon: 'el-icon-data-line' },
    children: [
      {
        path: 'debug_log',
        name: 'debug_log',
        component: () => import('@/views/log_view/DebugLog.vue'),
        meta: { title: 'Debugtalk日志', icon: 'table' }
      },
      {
        path: 'system_log',
        name: 'system_log',
        component: () => import('@/views/log_view/SystemLog.vue'),
        meta: { title: '系统日志', icon: 'table' }
      }
    ]
  },
  {
    path: '/debugtalk',
    component: Layout,
    redirect: 'debugtalk',
    name: 'Debugtalk',
    children: [
      {
        path: 'debugtalk',
        name: 'debugtalk',
        component: () => import('@/views/debugtalk'),
        meta: { title: '公共函数', icon: 'link' }
      }
    ]
  },

  {
    path: '/mock',
    component: Layout,
    redirect: 'mock_list',
    name: 'Mock',
    meta: { title: 'Mock', icon: 'el-icon-printer' },
    children: [
      {
        path: 'mock_list',
        name: 'mock_list',
        component: () => import('@/views/mock/MockList.vue'),
        meta: { title: 'Mock列表', icon: 'table' }
      },
      {
        path: 'create_mock',
        name: 'create_mock',
        component: () => import('@/views/mock/CreateMock.vue'),
        meta: { title: '新建Mock', icon: 'form' }
      },
      {
        path: 'update_mock',
        name: 'update_mock',
        hidden: true,
        component: () => import('@/views/mock/CreateMock.vue'),
        meta: { title: '编辑Mock', icon: 'form' }
      }
    ]
  },

  // {
  //   path: '/nested',
  //   component: Layout,
  //   redirect: '/nested/menu1',
  //   name: 'Nested',
  //   meta: {
  //     title: 'Nested',
  //     icon: 'nested'
  //   },
  //   children: [
  //     {
  //       path: 'menu1',
  //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
  //       name: 'Menu1',
  //       meta: { title: 'Menu1' },
  //       children: [
  //         {
  //           path: 'menu1-1',
  //           component: () => import('@/views/nested/menu1/menu1-1'),
  //           name: 'Menu1-1',
  //           meta: { title: 'Menu1-1' }
  //         },
  //         {
  //           path: 'menu1-2',
  //           component: () => import('@/views/nested/menu1/menu1-2'),
  //           name: 'Menu1-2',
  //           meta: { title: 'Menu1-2' },
  //           children: [
  //             {
  //               path: 'menu1-2-1',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
  //               name: 'Menu1-2-1',
  //               meta: { title: 'Menu1-2-1' }
  //             },
  //             {
  //               path: 'menu1-2-2',
  //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
  //               name: 'Menu1-2-2',
  //               meta: { title: 'Menu1-2-2' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'menu1-3',
  //           component: () => import('@/views/nested/menu1/menu1-3'),
  //           name: 'Menu1-3',
  //           meta: { title: 'Menu1-3' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'menu2',
  //       component: () => import('@/views/nested/menu2/index'),
  //       name: 'Menu2',
  //       meta: { title: 'menu2' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
