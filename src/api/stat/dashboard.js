import request from '@/utils/request'

export function dashboardStat(params) {
  return request({
    url: '/api/dashboard/',
    method: 'get',
    params
  })
}
