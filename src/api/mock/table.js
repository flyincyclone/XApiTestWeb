import request from '@/utils/request'

export function getMockList(params) {
  return request({
    url: '/api/mock_conf',
    method: 'get',
    params
  })
}

export function createMock(data) {
  return request({
    url: '/api/mock_conf/',
    method: 'post',
    data
  })
}

export function getMock(params) {
  return request({
    url: '/api/mock_conf/' + params.id + "/",
    method: 'get'
  })
}

export function updateMock(data) {
  return request({
    url: '/api/mock_conf/'+data.id+"/",
    method: 'put',
    data
  })
}

export function deleteMock(params) {
  return request({
    url: '/api/mock_conf/'+params.id+"/",
    method: 'delete'
  })
}

export function testMock(data) {
  return request({
    url: '/api/testmock/',
    method: 'post',
    data
  })
}
