import request from '@/utils/request'

export function getReportList(params) {
  return request({
    url: '/api/report/',
    method: 'get',
    params
  })
}

export function getReport(params) {
  return request({
    url: '/api/report/' + params + "/",
    method: 'get'
  })
}

export function deleteReport(params) {
  return request({
    url: '/api/report/'+params.id+"/",
    method: 'delete'
  })
}

export function downloadReport(params) {
  return request({
    url: '/api/reportdownload/' + params.id + "/",
    method: 'get',
    responseType: 'blob'
  })
}
