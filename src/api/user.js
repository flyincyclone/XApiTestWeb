import request from '@/utils/request'

export function register(data) {
  return request({
    url: '/api/register/',
    method: 'post',
    data
  })
}

export function login(data) {
  return request({
    url: '/api/login/',
    method: 'post',
    data
  })
}

export function getPassword(data) {
  return request({
    url: '/api/getpassword/',
    method: 'post',
    data
  })
}

export function updatePassword(data) {
  return request({
    url: '/api/updatepassword/',
    method: 'post',
    data
  })
}
// export function login(data) {
//   return request({
//     url: '/vue-admin-template/user/login',
//     method: 'post',
//     data
//   })
// }

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/api/logout',
    method: 'get'
  })
}

// export function logout() {
//   return request({
//     url: '/vue-admin-template/user/logout',
//     method: 'post'
//   })
// }
