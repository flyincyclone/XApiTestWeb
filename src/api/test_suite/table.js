import request from '@/utils/request'

export function getSuiteSumList(params) {
  return request({
    url: '/api/suite_sum/',
    method: 'get',
    params
  })
}

export function getSuiteList(params) {
  return request({
    url: '/api/suite/',
    method: 'get',
    params
  })
}

export function createSuite(data) {
  return request({
    url: '/api/suite/',
    method: 'post',
    data
  })
}

export function getSuite(params) {
  return request({
    url: '/api/suite/' + params.id + '/',
    method: 'get'
  })
}

export function updateSuite(data) {
  return request({
    url: '/api/suite/' + data.id + '/',
    method: 'put',
    data
  })
}

export function deleteSuite(params) {
  return request({
    url: '/api/suite/' + params.id + '/',
    method: 'delete'
  })
}
