import request from '@/utils/request'

export function getAuthSumList(params) {
  return request({
    url: '/api/auth_sum/',
    method: 'get',
    params
  })
}

export function getAuthList(params) {
  return request({
    url: '/api/auth/',
    method: 'get',
    params
  })
}

export function createAuth(data) {
  return request({
    url: '/api/auth/',
    method: 'post',
    data
  })
}

export function getAuth(params) {
  return request({
    url: '/api/auth/' + params.id + "/",
    method: 'get'
  })
}

export function updateAuth(data) {
  return request({
    url: '/api/auth/'+data.id+"/",
    method: 'put',
    data
  })
}

export function deleteAuth(params) {
  return request({
    url: '/api/auth/'+params.id+"/",
    method: 'delete'
  })
}

export function testAuth(data) {
  return request({
    url: '/api/testauth/',
    method: 'post',
    data
  })
}
