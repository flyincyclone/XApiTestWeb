import request from '@/utils/request'

export function loadJSON(data) {
  return request({
    url: '/api/load_json/',
    method: 'post',
    data
  })
}
