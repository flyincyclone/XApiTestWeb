import request from '@/utils/request'

export function getModuleSumList(params) {
  return request({
    url: '/api/module_sum/',
    method: 'get',
    params
  })
}

export function getModuleList(params) {
  return request({
    url: '/api/module/',
    method: 'get',
    params
  })
}

export function createModule(data) {
  return request({
    url: '/api/module/',
    method: 'post',
    data
  })
}

export function getModule(params) {
  return request({
    url: '/api/module/' + params.id + "/",
    method: 'get'
  })
}

export function updateModule(data) {
  return request({
    url: '/api/module/'+data.id+"/",
    method: 'put',
    data
  })
}

export function deleteModule(params) {
  return request({
    url: '/api/module/'+params.id+"/",
    method: 'delete'
  })
}
