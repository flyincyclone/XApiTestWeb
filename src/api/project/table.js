import request from '@/utils/request'

export function getProjectSumList(params) {
  return request({
    url: '/api/project_sum/',
    method: 'get',
    params
  })
}

export function getProjectList(params) {
  return request({
    url: '/api/project/',
    method: 'get',
    params
  })
}

export function createProject(data) {
  return request({
    url: '/api/project/',
    method: 'post',
    data
  })
}

export function getProject(params) {
  return request({
    url: '/api/project/' + params.id + "/",
    method: 'get'
  })
}

export function updateProject(data) {
  return request({
    url: '/api/project/'+data.id+"/",
    method: 'put',
    data
  })
}

export function deleteProject(params) {
  return request({
    url: '/api/project/'+params.id+"/",
    method: 'delete'
  })
}
