import request from '@/utils/request'

export function getConfigSumList(params) {
  return request({
    url: '/api/config_sum/',
    method: 'get',
    params
  })
}

export function getConfigList(params) {
  return request({
    url: '/api/config/',
    method: 'get',
    params
  })
}

export function createConfig(data) {
  return request({
    url: '/api/config/',
    method: 'post',
    data
  })
}

export function getConfig(params) {
  return request({
    url: '/api/config/' + params.id + '/',
    method: 'get'
  })
}

export function updateConfig(data) {
  return request({
    url: '/api/config/' + data.id + '/',
    method: 'put',
    data
  })
}

export function deleteConfig(params) {
  return request({
    url: '/api/config/' + params.id + '/',
    method: 'delete'
  })
}
