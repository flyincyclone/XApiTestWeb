import request from '@/utils/request'

export function getCaseSumList(params) {
  return request({
    url: '/api/case_sum/',
    method: 'get',
    params
  })
}

export function getCaseList(params) {
  return request({
    url: '/api/case/',
    method: 'get',
    params
  })
}

export function createCase(data) {
  return request({
    url: '/api/case/',
    method: 'post',
    data
  })
}

export function getCase(params) {
  return request({
    url: '/api/case/' + params.id + '/',
    method: 'get'
  })
}

export function updateCase(data) {
  return request({
    url: '/api/case/' + data.id + '/',
    method: 'put',
    data
  })
}

export function deleteCase(params) {
  return request({
    url: '/api/case/' + params.id + '/',
    method: 'delete'
  })
}
