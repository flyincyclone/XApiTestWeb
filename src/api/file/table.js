import request from '@/utils/request'

export function getFileList(params) {
  return request({
    url: '/api/file/',
    method: 'get',
    params
  })
}

export function uploadFile(formData) {
  return request({
    url: '/api/file/',
    method: 'post',
    headers: {'Content-Type': 'multipart/form-data'},
    data: formData
  })
}

export function deleteFile(params) {
  return request({
    url: '/api/file/'+params.id+"/",
    method: 'delete'
  })
}

export function downloadFile(params) {
  return request({
    url: '/api/file/' + params.id + "/",
    method: 'get',
    responseType: 'blob'
  })
}
