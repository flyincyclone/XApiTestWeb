import request from '@/utils/request'

export function getLogs(params) {
  return request({
    url: '/api/logs/',
    method: 'get',
    params
  })
}

