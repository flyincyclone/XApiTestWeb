import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/api/case/',
    method: 'get',
    params
  })
}
