import request from '@/utils/request'

export function runTest(data) {
  return request({
    url: '/api/run_test/',
    method: 'post',
    data
  })
}
