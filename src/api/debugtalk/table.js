import request from '@/utils/request'

export function getDebugtalk() {
  return request({
    url: '/api/debugtalk/',
    method: 'get'
  })
}

