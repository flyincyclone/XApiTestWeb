import request from '@/utils/request'

export function getScheduleList(params) {
  return request({
    url: '/api/schedule/',
    method: 'get',
    params
  })
}

export function createSchedule(data) {
  return request({
    url: '/api/schedule/',
    method: 'post',
    data
  })
}

export function getSchedule(params) {
  return request({
    url: '/api/schedule/' + params.id + '/',
    method: 'get'
  })
}

export function updateSchedule(data) {
  return request({
    url: '/api/schedule/' + data.id + '/',
    method: 'put',
    data
  })
}

export function deleteSchedule(params) {
  return request({
    url: '/api/schedule/' + params.id + '/',
    method: 'delete'
  })
}
