import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import CodeEditor from 'bin-code-editor'
Vue.use(CodeEditor)

import common from './utils/common'
Vue.prototype.$common = common
Vue.prototype.$eventBus = new Vue()
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

Vue.prototype.setLocalValue = function (name, value) {
    if (window.localStorage) {
        localStorage.setItem(name, value);
    } else {
        alert('This browser does NOT support localStorage');
    }
};
Vue.prototype.getLocalValue = function (name) {
    const value = localStorage.getItem(name);
    if (value) {
        return value;
    } else {
        return '';
    }
};

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created() {
    if (this.getLocalValue("token") === null) {
        this.setLocalValue("token", "");
    }
    if (this.getLocalValue("acount") === null) {
        this.setLocalValue("acount", "");
    }
    // if (this.getLocalValue("routerName") === null) {
    //     this.setLocalValue("routerName", "ProjectList");
    // }
    this.$store.commit("user/SET_TOKEN", this.getLocalValue("token"));
    this.$store.commit("user/SET_ACCOUNT", this.getLocalValue("acount"));
    // this.$store.commit("setRouterName", this.getLocalValue("routerName"));
  }
})
