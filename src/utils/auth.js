const TokenKey = 'token'
const Accountkey = 'account'

export function getToken() {
  return window.localStorage.getItem(TokenKey)
}

export function setToken(value) {
  window.localStorage.setItem(TokenKey, JSON.stringify(value))
}

export function removeToken() {
  window.localStorage.removeItem(TokenKey)
  window.localStorage.removeItem(Accountkey)
}
