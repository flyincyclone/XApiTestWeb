/**
 * Created by Xuph on 2023-02-02.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validLoginUsername(str) {
    if (str < 4) {
      return false
    }
  return true
}

export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  if (valid_map.indexOf(str.trim()) < 0){
      if (str.length < 4) {
      return false
  }else{
        return true
      }
  }else{
        return true
  }
}

export function validPassword(str) {
    if (str.length < 6) {
      return false
    } else {
        return true
    }
}


export function validEmail(str) {
  let email_reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$")
  if (!email_reg.test(str)) {
    return false
  } else{
    return true
  }
}
