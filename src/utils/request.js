import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import router from '@/router'

// create an axios instance
const service = axios.create({
  baseURL: '', // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 20000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    if (config.url.indexOf('/user/login') >= 0 || config.url.indexOf('/user/info') >= 0 || config.url.indexOf('/user/logout') >= 0) {
      config.url = process.env.VUE_APP_BASE_API + config.url
    } else {
      // eslint-disable-next-line no-self-assign
      config.url = config.url
    }
    // do something before request is sent

    // config.headers.post['X-Requested-With'] = 'XMLHttpRequest'

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data
    const headers = response.headers
    // 此类为下载流文件，不拦截
    if (headers['content-type'] === 'application/octet-stream' || response.config.responseType === 'blob' || headers['content-type'] === 'text/html; charset=utf-8') {
      return res
    }
    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 20000 && res.code !== '000000') {
      if (res.results) {
        return res
      }
      Message({
        message: res.message || res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    const msg = error.message
    if (msg.indexOf('401') !== -1) {
      window.localStorage.removeItem('token')
      router.push({ path: '/login' })
      Message({
        message: 'token过期，需重新登陆',
        type: 'info',
        duration: 3 * 1000
      })
    } else {
      console.log('err' + error) // for debug
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    }
  }
)

export default service
