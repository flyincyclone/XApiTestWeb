var common = {
  formatDateTime: function(dateTime) {
    if (!dateTime && typeof dateTime !== 'number') {
      return ''
    }
    var localTime = ''
    dateTime = new Date(dateTime).getTime()
    const offset = (new Date()).getTimezoneOffset()
    localTime = (new Date(dateTime - offset * 60000)).toISOString()
    localTime = localTime.substr(0, localTime.lastIndexOf('.'))
    localTime = localTime.replace('T', ' ')
    return localTime
  },
  formatNumber: function(number = 0, numLength = 3) {
    let length = number.toString()
    let num = '0.'
    if (length.indexOf('.') === -1) {
      return number
    }
    length = length.split('.')[1].length < numLength ? numLength : length.split('.')[1].length
    for (var i = 0; i < length; i++) {
      num += '0'
    }
    num += '1'
    return Math.floor((Number(number) + Number(num)) * Math.pow(10, numLength)) / Math.pow(10, numLength)
  }
}

export default common
